//
//  EntitiesKeys.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#ifndef EntitiesKeys_h
#define EntitiesKeys_h

//message
#define kStatus @"status"
#define kError @"error"
#define kMessage @"message"
#define kDate @"date"


#endif /* EntitiesKeys_h */
