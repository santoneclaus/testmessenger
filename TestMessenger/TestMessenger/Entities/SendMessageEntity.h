//
//  SendMessageEntity.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendMessageEntity : NSObject
/**
 message key
 */
@property (nonatomic, strong, readonly) NSString *func;
/**
 message text
 */
@property (nonatomic, strong, nullable) NSString *txt;
/**
 first and last name separated by a space
 */
@property (nonatomic, strong, readonly) NSString *nm;
/**
 
 */
@property (nonatomic, strong, readonly) NSString *user_key;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithName:(NSString *)name
                     surname:(NSString *)surname;

- (NSDictionary *)requestDictionary;

@end

NS_ASSUME_NONNULL_END
