//
//  MessageEntity.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "MessageEntity.h"

@interface MessageEntity ()

@end

@implementation MessageEntity

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.status forKey:kStatus];
    if (nil != self.error) {
        [aCoder encodeObject:self.error forKey:kError];
    }
    if (nil != self.message) {
        [aCoder encodeObject:self.message forKey:kMessage];
    }
    if (nil != self.date) {
        [aCoder encodeObject:self.date forKey:kDate];
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    TMSafeInitSelf(self = [super init]);
    
    self.status = [aDecoder decodeIntegerForKey:kStatus];
    self.date = [aDecoder decodeObjectForKey:kDate];
    self.error = [aDecoder decodeObjectForKey:kError];
    self.message = [aDecoder decodeObjectForKey:kMessage];
    
    return self;
}

- (instancetype)copyWithZone:(NSZone *)zone {
    MessageEntity *item = [[MessageEntity allocWithZone:zone] init];
    item.status = self.status;
    item.date = self.date ? [self.date copy] : nil;
    item.error = self.error ? [self.error copy] : nil;
    item.message = self.message ? [self.message copy] : nil;

    return item;
}

- (BOOL)isEqual:(id)object {
    TMCheckRet(![object isKindOfClass:[self class]], NO);
    MessageEntity *item = object;
    if (nil != item) {
        return [(item.message ? : @"") isEqualToString:(self.message ? : @"")] && [(item.error ? : @"") isEqualToString:(self.error ? : @"")] && (self.status == item.status) && ((self.date ? self.date.integerValue : 0) == (item.date ? item.date.integerValue : 0));
    }
    return NO;
}

- (void)setMessage:(NSString<Optional> *)message {
    _message = message;
    if (nil == self.date || 0 == self.date.integerValue) {
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        self.date = @(currentTime);
    }
}

- (NSString *)getTimeString {
//    TMCheckRet(nil != self.timeStr, self.timeStr);
    
    return timeBefor([NSDate dateWithTimeIntervalSince1970:(self.date ? self.date.integerValue : 0)]);
}

- (NSString<Optional> *)error {
    TMCheckRet(self.status == 0 || (nil != _error), _error);
    return @"Произошла ошибка";
}

NSString *timeBefor(NSDate *date) {
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:date];
    TMCheckRet(0 == timeInterval, @"");
    TMCheckRet(0 == timeInterval, @"");
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setFirstWeekday:2];
    
    NSTimeInterval todayTimeInterval = [calendar ordinalityOfUnit:NSCalendarUnitSecond inUnit:NSCalendarUnitDay forDate:[NSDate date]];
    timeInterval -= todayTimeInterval;
    
    static NSDateFormatter *timeFormatter = nil;
    if (!timeFormatter) timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    
    
    if (timeInterval <= 0) {
        timeInterval += todayTimeInterval;
        NSInteger minutes = timeInterval/60;
        NSInteger lastDigit, lastButOneDigit;
        
        if (minutes == 0) {
            return @"Только что";
        }
        
        if (minutes < 60) {
            lastDigit = minutes%10;
            lastButOneDigit = (minutes/10)%10;
            
            if (lastButOneDigit == 1)
                return [NSString stringWithFormat:@"%ld минут назад", (long)minutes];
            
            if (lastDigit == 1)
                return [NSString stringWithFormat:@"%ld минуту назад", (long)minutes];
            
            if ((lastDigit <=4 ) && (lastDigit >= 2))
                return [NSString stringWithFormat:@"%ld минуты назад", (long)minutes];
            
            return [NSString stringWithFormat:@"%ld минут назад", (long)minutes];
        }
        
        minutes /= 60;
        lastDigit = minutes%10;
        lastButOneDigit = (minutes/10)%10;
        
        if (lastButOneDigit == 1)
            return [NSString stringWithFormat:@"%ld часов назад", (long)minutes];
        if (lastDigit == 1)
            return [NSString stringWithFormat:@"%ld час назад", (long)minutes];
        if ((lastDigit<=4) && (lastDigit>=2))
            return [NSString stringWithFormat:@"%ld часа назад", (long)minutes];
        
        return [NSString stringWithFormat:@"%ld часов назад", (long)minutes];
    }
    if (timeInterval<=86400)
        return [NSString stringWithFormat:@"Вчера в %@", [timeFormatter stringFromDate:date]];
    if (timeInterval<=86400*2)
        return [NSString stringWithFormat:@"Позавчера в %@", [timeFormatter stringFromDate:date]];
    
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter) dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:date];
}

@end
