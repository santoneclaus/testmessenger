//
//  MessageEntity.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <JSONModel/JSONModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageEntity : JSONModel <NSCopying,NSCoding>

/**
 message sending status
 0 - good
 1 - error
 */
@property (nonatomic, assign) NSInteger status;
//server error
@property (nonatomic, strong, nullable) NSString <Optional> *error;
//sending message
@property (nonatomic, strong, nullable) NSString <Optional> *message;
//date of sending message
@property (nonatomic, strong) NSNumber <Optional> *date;

- (NSString*)getTimeString;

@end

NS_ASSUME_NONNULL_END
