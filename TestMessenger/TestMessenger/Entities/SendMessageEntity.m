//
//  SendMessageEntity.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "SendMessageEntity.h"
#import "NSString+Crypto.h"

@interface SendMessageEntity ()

@property (nonatomic, strong) NSString *func;
@property (nonatomic, strong) NSString *nm;

@end

@implementation SendMessageEntity

- (instancetype)initWithName:(NSString *)name surname:(NSString *)surname {
    TMSafeInitSelf(self = [super init]);
    
    [self configure];
    self.nm = [NSString stringWithFormat:@"%@ %@", name ? : @"", surname ? : @""];
    
    return self;
}

- (instancetype)init {
    TMSafeInitSelf(self = [super init]);
    
    [self configure];
    
    return self;
}

- (void)configure {
    self.func = @"send_msg";
    self.txt = @"";
}

- (NSString *)user_key {
    if (nil == self.txt) {
        self.txt = @"";
    }
    NSString *user_key = [[self.txt stringByAppendingString:self.nm] MD5String];
    return user_key;
}

- (NSDictionary *)requestDictionary {
    NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionary];
    requestDictionary[@"func"] = self.func;
    requestDictionary[@"txt"] = self.txt;
    requestDictionary[@"nm"] = self.nm;
    requestDictionary[@"user_key"] = [self user_key];

    return requestDictionary;
}

@end
