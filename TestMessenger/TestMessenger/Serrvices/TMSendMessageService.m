//
//  TMSendMessageService.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMSendMessageService.h"
#import <AFNetworking/AFNetworking.h>
#import "MessageEntity.h"

@interface TMSendMessageService ()

@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) AFHTTPSessionManager *manager;

@end

@implementation TMSendMessageService

- (void)configureManager {
    self.manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableSet *set = [[NSMutableSet alloc] initWithSet:self.manager.responseSerializer.acceptableContentTypes];
    [set addObject:@"text/plain"];
    [set addObject:@"text/html"];
    self.manager.responseSerializer.acceptableContentTypes = set;
}

- (instancetype)initWithUrlString:(NSString *)urlString {
    TMSafeInitSelf(self = [super init]);
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    self.urlString = urlString;
    [self configureManager];

    return self;
}

- (void)sendMessageWithEntity:(SendMessageEntity *)sendMessageEntity
                     callBack:(nullable void (^)(MessageEntity * _Nonnull))callBack{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        if (nil != callBack) {
            MessageEntity *messageEntity = [[MessageEntity alloc] init];
            messageEntity.status = 1;
            messageEntity.message = sendMessageEntity.txt;
            messageEntity.error = @"Ошибка подключения к сети интернет";
            callBack(messageEntity);
        }
        return;
    }
    NSMutableURLRequest *request =
    [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST"
                                                  URLString:self.urlString
                                                 parameters:[sendMessageEntity requestDictionary]
                                                      error:nil];


    NSString *sendText = sendMessageEntity.txt;
    NSURLSessionDataTask *dataTask =
        [self.manager dataTaskWithRequest:request
                           uploadProgress:nil
                         downloadProgress:nil
                        completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (nil != callBack) {
                MessageEntity *messageEntity;
                if (nil != error) {
                    //TODO: errormanager
                    messageEntity = [[MessageEntity alloc] init];
                    messageEntity.status = 1;
                    messageEntity.error = error.description;
                } else {
                    NSDictionary *errordict = @{@"status" : @"1", @"error" : @"Ошибка парсинга"};
                    NSDictionary *responseObject_ = [responseObject isKindOfClass:[NSDictionary class]] ? responseObject : errordict;
                    messageEntity = [[MessageEntity alloc] initWithDictionary:responseObject_
                                                                        error:nil];
                    if (nil == messageEntity) {
                        messageEntity = [[MessageEntity alloc] initWithDictionary:errordict
                                                                            error:nil];
                    }
                }
                messageEntity.message = sendText;
                runOnMainQueue(^{
                    callBack(messageEntity);
                });
            }
        }];
    
    
    [dataTask resume];
}

@end
