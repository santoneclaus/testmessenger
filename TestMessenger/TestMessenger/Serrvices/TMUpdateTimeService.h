//
//  TMUpdateTimeService.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//callback type
typedef BOOL (^TMSimpleBoolRetCallBack)(void);

@protocol TMUpdateTimeDelegate;
/**
  to update time
 */
@protocol TMUpdateTimeDelegate <NSObject>
- (void)updateDate;
@end

@interface TMUpdateTimeService : NSObject

/**
  Initializes an instance of a update service

  @return nil as this method is unavailable
*/
- (instancetype)init NS_UNAVAILABLE;

/**
  Initializes an instance of a update service
  
  @param callBack a callback This block return bool value and not takes
  
  @return TMUpdateTimeService
*/
- (instancetype)initWithCallBack:(nullable TMSimpleBoolRetCallBack)callBack;

- (void)reupdateTable:(UITableView *)tableView;
- (void)startUpdateTable:(UITableView *)tableView;
- (void)stoopUpdateTable;

@end

NS_ASSUME_NONNULL_END
