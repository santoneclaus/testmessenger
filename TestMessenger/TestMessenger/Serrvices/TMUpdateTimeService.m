//
//  TMUpdateTimeService.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMUpdateTimeService.h"

@interface TMUpdateTimeService ()

@property (nonatomic, strong) TMSimpleBoolRetCallBack callBack;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSObject *monitor;
@property (nonatomic, strong) NSTimer *updateTimeTimer;
@property (nonatomic, assign) NSInteger secondForUpdate;

@end

@implementation TMUpdateTimeService

- (instancetype)init {
    return nil;
}

- (instancetype)initWithCallBack:(TMSimpleBoolRetCallBack)callBack {
    TMSafeInitSelf(self = [super init]);
    
    self.callBack = callBack;
    self.monitor = [[NSObject alloc] init];
    
    return self;
}

- (void)dealloc {
    [self stoopUpdateTable];
}

- (void)reupdateTable:(UITableView *)tableView {
    @synchronized (self.monitor) {
        [self startUpdateTable:tableView];
        [self updateTime:self.updateTimeTimer];
    }
}

- (void)startUpdateTable:(UITableView *)tableView {
    @synchronized (self.monitor) {
        [self stoopUpdateTable];
        self.tableView = tableView;
        if (nil == self.updateTimeTimer) {
            self.updateTimeTimer = [NSTimer scheduledTimerWithTimeInterval:self.secondForUpdate target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        }
    }
}
- (void)stoopUpdateTable {
    @synchronized (self.monitor) {
        self.secondForUpdate = 5;
        self.tableView = nil;
        if (nil != self.updateTimeTimer) {
            [self.updateTimeTimer invalidate];
            self.updateTimeTimer = nil;
        }
    }
}

- (void)updateTime:(NSTimer *)timer {
    @synchronized (self.monitor) {
        TMCheckRetVoid((nil == self.tableView) || (nil == self.callBack) || !self.callBack());
        runOnMainQueue(^{
            NSArray *cells = [self.tableView visibleCells];
            for (id <TMUpdateTimeDelegate>cell in cells) {
                if ([cell conformsToProtocol:@protocol(TMUpdateTimeDelegate)]) {
                    [cell updateDate];
                }
            }
        });
        if (self.secondForUpdate < 60) {
            [self.updateTimeTimer invalidate];
            if (self.secondForUpdate < 10) {
                self.secondForUpdate = 10;
            } else if (self.secondForUpdate < 15) {
                self.secondForUpdate = 15;
            } else if (self.secondForUpdate < 30) {
                self.secondForUpdate = 10;
            } else {
                self.secondForUpdate = 60;
            }
            self.updateTimeTimer = [NSTimer scheduledTimerWithTimeInterval:self.secondForUpdate target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        }
    }
}
@end
