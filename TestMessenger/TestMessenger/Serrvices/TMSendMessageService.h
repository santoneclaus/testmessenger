//
//  TMSendMessageService.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SendMessageEntity.h"
#import "MessageEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMSendMessageService : NSObject

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithUrlString:(nonnull NSString *)urlString;

- (void)sendMessageWithEntity:(nonnull SendMessageEntity *)sendMessageEntity
                     callBack:(nullable void (^)(MessageEntity *messageEntity))callBack;

@end

NS_ASSUME_NONNULL_END
