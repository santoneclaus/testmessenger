//
//  CommentView.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMViewWithNib.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^TMTextCallBack)(NSString *text);

/**
 'CommentView'  View for writing comments with the ability to change the height (when setting the number of lines and the height constan - 'heightLayautConstraint')
 */
@interface CommentView : TMViewWithNib
/**
 this LayoutConstraint need for change view height
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLayautConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;
/**
 @def NO
 */
@property (nonatomic, assign) BOOL disableAnimation;
/**
 the number of lines to change the height
    from 1 to 20
 @def 5
 */
@property (nonatomic, assign) NSUInteger maxHeightLine;
/**
 maximum comment text length
 @def 255
 
 */
@property (nonatomic, assign) NSUInteger maxCommentLenght;
/**
 clearr text view
 */
- (void)clearComment;
/**
 called after clicking send
 */
@property (nonatomic, strong)TMTextCallBack sendCommentCallBack;
/**
 calculate height
 */
- (void)updateHeight;

@property (nonatomic, assign) BOOL disable;

@end

NS_ASSUME_NONNULL_END
