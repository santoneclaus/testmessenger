//
//  TMViewWithNib.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMViewWithNib.h"

@interface TMViewWithNib ()
@property (strong, nonatomic) UIView* nibRoot;

- (void)initializeWithFrame:(CGRect)frame;
@end

@implementation TMViewWithNib

- (instancetype)init {
    TMSafeInitSelf(self = [super initWithFrame:CGRectZero]);
    
    [self initializeWithFrame:CGRectZero];
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    TMSafeInitSelf(self = [super initWithFrame:frame]);
    
    [self initializeWithFrame:frame];
    
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder {
    TMSafeInitSelf(self = [super initWithCoder:aDecoder]);
    
    [self initializeWithFrame:CGRectZero];
    
    return self;
}

- (void)initializeWithFrame:(CGRect)frame {
    TMAssertRetVoid(nil == self.nibRoot);
    
    NSString* nibName = [self nibName];
    
    self.nibRoot = [[[NSBundle mainBundle] loadNibNamed:nibName
                                                  owner:self
                                                options:nil] firstObject];
    
    TMAssertRetVoid(self.nibRoot != nil);
    
    if (CGRectIsEmpty(frame)) {
        self.bounds = self.nibRoot.bounds;
        self.frame = self.nibRoot.frame;
    } else {
        self.nibRoot.frame = self.bounds;
    }
    self.opaque = self.nibRoot.opaque;
    
    [self addSubview:self.nibRoot];
    
    self.nibRoot.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.nibRoot setTranslatesAutoresizingMaskIntoConstraints:YES];
}

- (void)sizeToNibFit {
    CGSize size = [self.nibRoot calculateSizeToFitSubviews];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self.nibRoot setFrame:CGRectMake(self.nibRoot.frame.origin.x, self.nibRoot.frame.origin.y, frame.size.width, frame.size.height)];
}

- (void)sizeToNibFitWithLayout {
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    [self sizeToNibFit];
}

- (NSString*)nibName {
    return NSStringFromClass([self class]);
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    [super setBackgroundColor:backgroundColor];
    [self.nibRoot setBackgroundColor:backgroundColor];
}

@end

@implementation UIView (Extension)

- (CGSize)calculateSizeToFitSubviews {
    float w = 0;
    float h = 0;
    
    for (UIView* v in [self subviews]) {
        float fw = v.frame.origin.x + v.frame.size.width;
        float fh = v.frame.origin.y + v.frame.size.height;
        w = MAX(fw, w);
        h = MAX(fh, h);
    }
    return CGSizeMake(w, h);
}

@end
