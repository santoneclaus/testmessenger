//
//  TMViewWithNib.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 if need add custom view
 
 - set cass for custom view in files owner field
 */
@interface TMViewWithNib : UIView

@property (nonatomic, readonly) UIView* nibRoot;

- (void)sizeToNibFit;
- (void)sizeToNibFitWithLayout;

- (NSString*)nibName;

@end

@interface UIView (Extension)

- (CGSize)calculateSizeToFitSubviews;

@end
