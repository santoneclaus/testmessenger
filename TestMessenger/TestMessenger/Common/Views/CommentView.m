//
//  CommentView.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "CommentView.h"

#define kMaxCommentLenght 255

@interface CommentView () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeightLayautConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placeholderLabelHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (assign, nonatomic) CGFloat minTextViewHeight;
@property (assign, nonatomic) CGFloat maxTextViewHeight;

@end

@implementation CommentView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureUI];
}

- (void)configureUI {
    self.disableAnimation = YES;
    self.commentTextView.text = @"";
    self.commentTextView.delegate = self;
    self.maxCommentLenght = kMaxCommentLenght;
    
    self.maxHeightLine = 5;
    
    self.sendButton.enabled = NO;
    
    CGFloat lineWidth = 1.0/[UIScreen mainScreen].scale;
    self.separatorHeightLayautConstraint.constant = lineWidth;
    self.backView.layer.borderColor = RGB(208, 208, 208).CGColor;
    self.backView.layer.borderWidth = lineWidth;
    
    UIImage *sendImage = self.sendButton.imageView.image;
    [self.sendButton setImage:sendImage forState:UIControlStateDisabled];
    [self.sendButton setImage:[sendImage imageWithColor:RGB(3, 169, 244)] forState:UIControlStateNormal];
        
    self.disableAnimation = NO;
}

- (void)setMaxHeightLine:(NSUInteger)maxHeightLine  {
    _maxHeightLine = MAX(1, MIN(25, maxHeightLine));
    NSInteger lineNumber = 1;
    NSString *text = @"Введите текст сообщения";
    while (lineNumber < _maxHeightLine) {
        text = [text stringByAppendingString:@"\n"];
        lineNumber ++;
    }
    self.placeholderLabel.numberOfLines = 0;
    self.placeholderLabel.font = self.commentTextView.font;
    self.placeholderLabel.text = text;
    [self.placeholderLabel sizeToFit];
    
    CGFloat delta = (self.placeholderLabel.frame.origin.y - self.commentTextView.frame.origin.y)*2;
    self.maxTextViewHeight = self.placeholderLabel.frame.size.height + delta;
    self.minTextViewHeight = self.placeholderLabel.frame.size.height/_maxHeightLine + delta;
}

- (void)clearComment {
    self.commentTextView.text = @"";
    self.sendButton.enabled = NO;
    self.commentTextView.contentSize = CGSizeZero;
    [self updateHeight];
}

- (IBAction)sendTapped:(UIButton *)sender {
    sender.enabled = NO;
    if (nil != self.sendCommentCallBack) {
        self.sendCommentCallBack(self.commentTextView.text);
    }
    sender.enabled = YES;
    self.sendButton.enabled = 0 < self.commentTextView.text.length;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *totalString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    self.placeholderLabel.hidden = (0 < totalString.length);
    self.sendButton.enabled = self.placeholderLabel.hidden;
    if (totalString.length > self.maxCommentLenght) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
//    CGRect line = [textView caretRectForPosition:textView.selectedTextRange.start];
//    CGFloat overflow = line.origin.y + line.size.height - ( textView.contentOffset.y + textView.bounds.size.height
//                                                           - textView.contentInset.bottom - textView.contentInset.top );
//    if ( overflow > 0 ) {
//        CGPoint offset = textView.contentOffset;
//        offset.y += overflow;
//        [UIView animateWithDuration:.2 animations:^{
//            [textView setContentOffset:offset];
//        }];
//    }
    [self updateHeight];
}

- (void)updateHeight {
    self.placeholderLabel.hidden = (0 < self.commentTextView.text.length);
    CGFloat newHeight = self.commentTextView.contentSize.height;
    newHeight = MIN(newHeight, self.maxTextViewHeight);
    newHeight = MAX(self.minTextViewHeight, newHeight);
    
    if(newHeight != self.commentTextView.frame.size.height) {
        self.commentTextView.frame = CGRectMake(self.commentTextView.frame.origin.x, self.commentTextView.frame.origin.y, self.commentTextView.frame.size.width, newHeight);
    }

    TMCheckRetVoid(nil == self.heightLayautConstraint);
    
    self.heightLayautConstraint.constant = CGRectGetMaxY(self.commentTextView.frame) + self.bottomSpaceConstraint.constant;
    if (self.disableAnimation) {
        self.commentTextView.frame = CGRectSizeHeight(self.commentTextView.frame, newHeight);
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            [self.superview layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.commentTextView.frame = CGRectSizeHeight(self.commentTextView.frame, newHeight);
        }];
    }
}

- (void)setDisable:(BOOL)disable {
    _disable = disable;
    [self.commentTextView setEditable:!disable];
    self.sendButton.enabled = !disable;
}

@end
