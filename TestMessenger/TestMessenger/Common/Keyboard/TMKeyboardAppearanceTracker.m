//
//  TMKeyboardAppearanceTracker.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMKeyboardAppearanceTracker.h"

@interface TMKeyboardAppearanceTracker ()

@property (nonatomic, strong) NSLayoutConstraint* bottomMarginConstraint;
@property (nonatomic, strong) UIView* parent;
@property (nonatomic, assign) UIEdgeInsets defaultContentInsets;
@property (nonatomic, assign) BOOL storeDefaultInsets;

@end

@implementation TMKeyboardAppearanceTracker

- (instancetype)initWithHeightConstraint:(NSLayoutConstraint*)constraint parent:(UIView*)parent {
    TMSafeInitSelf(self = [super init]);
    
    self.bottomMarginConstraint = constraint;
    self.parent = parent;
    self.enableHide = YES;
    self.bottomSpace = 0;
    
    [self addNotifications];
    
    return self;
}

- (void)addNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc {
    [self removeNotifications];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardFrame.size.height - self.bottomSpace;
    CGFloat animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIScrollView* scrollView = [self findScrollView];
    if (nil != scrollView) {
        if (!self.storeDefaultInsets) {
            self.storeDefaultInsets = YES;
            self.defaultContentInsets = scrollView.contentInset;
        }
        
        [scrollView layoutIfNeeded];
        [UIView animateWithDuration:animationDuration
                         animations:^{
                             UIEdgeInsets contentInset = scrollView.contentInset;
                             contentInset.bottom = keyboardHeight;
                             scrollView.contentInset = contentInset;
                             [scrollView layoutIfNeeded];
                             if (nil != self.keyboardAnimatedBlock) {
                                 self.keyboardAnimatedBlock(keyboardHeight);
                             }
                         }
                         completion:^(BOOL finished) {
                             [self scrollview:scrollView movedOn:keyboardHeight];
                         }];
    } else {
        self.bottomMarginConstraint.constant = keyboardHeight;
        [UIView animateWithDuration:animationDuration
                         animations:^{
                             [self.parent.superview layoutIfNeeded];
                             [self.parent layoutIfNeeded];
                             if (nil != self.keyboardAnimatedBlock) {
                                 self.keyboardAnimatedBlock(keyboardHeight);
                             }
                         }];
    }
}

- (UIScrollView*)findScrollView {
    if ([self.bottomMarginConstraint.firstItem isKindOfClass:[UIScrollView class]]) {
        return (UIScrollView*)self.bottomMarginConstraint.firstItem;
    }
    if ([self.bottomMarginConstraint.secondItem isKindOfClass:[UIScrollView class]]) {
        return (UIScrollView*)self.bottomMarginConstraint.secondItem;
    }
    if ((nil != self.parent) && ([self.parent.superview isKindOfClass:[UIScrollView class]])) {
        return (UIScrollView*)self.parent.superview;
    }
    return nil;
}

- (UIView*)findFirstResponder:(UIView*)view {
    if ([view isFirstResponder]) {
        return view;
    }
    
    for (UIView* subview in view.subviews) {
        UIView* firstResponder = [self findFirstResponder:subview];
        if (nil != firstResponder) {
            return firstResponder;
        }
    }
    
    return nil;
}

- (void)scrollview:(UIScrollView*)scrollView movedOn:(CGFloat)keyboardHeight {
    TMCheckRetVoid(nil == scrollView);
    
    UIView* firstResponder = [self findFirstResponder:scrollView];
    TMCheckRetVoid(nil == firstResponder);
    
    CGFloat height = firstResponder.frame.size.height;
    CGFloat y = firstResponder.frame.origin.y - 10;
    
    if (![self.parent isEqual:[firstResponder superview]]) {
        y = [scrollView convertPoint:CGPointMake(0, height) fromView:firstResponder].y;
    }
}

- (void)keyboardWillHide:(NSNotification*)notification {
    if (self.enableHide) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGFloat animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        UIScrollView* scrollView = [self findScrollView];
        
        if (nil != scrollView) {
            [scrollView layoutIfNeeded];
            [UIView animateWithDuration:animationDuration
                             animations:^{
                                 UIEdgeInsets contentInset = self.defaultContentInsets;
                                 scrollView.contentInset = contentInset;
                                 [scrollView layoutIfNeeded];
                                 if (nil != self.keyboardAnimatedBlock) {
                                     self.keyboardAnimatedBlock(0);
                                 }
                             }];
        } else {
            self.bottomMarginConstraint.constant = 0;
            [UIView animateWithDuration:animationDuration
                             animations:^{
                                 [self.parent.superview layoutIfNeeded];
                                 [self.parent layoutIfNeeded];
                                 if (nil != self.keyboardAnimatedBlock) {
                                     self.keyboardAnimatedBlock(0);
                                 }
                             }];
        }
    }
}

@end

