//
//  TMKeyboardAppearanceTracker.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^KeyboardAnimatedBlock)(CGFloat keyboardHeight);

NS_ASSUME_NONNULL_BEGIN

@interface TMKeyboardAppearanceTracker : NSObject

- (instancetype)initWithHeightConstraint:(NSLayoutConstraint*)constraint parent:(UIView*)parent;

@property (nonatomic, assign) BOOL enableHide;
@property (nonatomic, assign) CGFloat bottomSpace;
@property (nonatomic, strong, nullable) KeyboardAnimatedBlock keyboardAnimatedBlock;

@end

NS_ASSUME_NONNULL_END
