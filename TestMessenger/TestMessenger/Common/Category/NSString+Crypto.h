//
//  NSString+Crypto.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSString (Crypto)
- (NSString *)MD5String;
@end
