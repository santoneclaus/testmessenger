//
//  CommonSettings.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "CommonSettings.h"

@interface CommonSettings ()

@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) SendMessageEntity *sendMessageEntity;

@end

@implementation CommonSettings

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static CommonSettings *commonSettings;
    dispatch_once(&onceToken, ^{
        commonSettings = [[CommonSettings alloc] init];
        [commonSettings configure];
    });
    return commonSettings;
}

- (void)configure {
    self.baseURL = @"https://ios-test.alef.im/index.php";
    self.sendMessageEntity = [[SendMessageEntity alloc] initWithName:@"FirstName" surname:@"LastName"];
}

+ (NSString *)baseURL {
    return [CommonSettings sharedInstance].baseURL;
}

+ (SendMessageEntity *)sendMessageEntityWithText:(NSString *)text {
    SendMessageEntity *sendMessageEntity = [CommonSettings sharedInstance].sendMessageEntity;
    sendMessageEntity.txt = text;
    return sendMessageEntity;
}


@end
