//
//  GDCHelper.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GDCHelper : NSObject

void runOnMainQueueWithoutDeadlocking(void (^block)(void));
void runOnMainQueue(void (^block)(void));

@end

NS_ASSUME_NONNULL_END
