//
//  UIHelper.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#ifndef UIHelper_h
#define UIHelper_h

#import <UIKit/UIKit.h>

// Назначить rect новый origin.x
CG_INLINE CGRect CGRectOriginX(CGRect rect, CGFloat originX);

CG_INLINE CGRect
CGRectOriginX(CGRect rect, CGFloat originX)
{
    rect.origin.x = originX;
    return rect;
}

// Назначить rect новый origin.y
CG_INLINE CGRect CGRectOriginY(CGRect rect, CGFloat originY);

CG_INLINE CGRect
CGRectOriginY(CGRect rect, CGFloat originY)
{
    rect.origin.y = originY;
    return rect;
}

// Назначить rect новый origin
CG_INLINE CGRect CGRectOrigin(CGRect rect, CGFloat originX, CGFloat originY);

CG_INLINE CGRect
CGRectOrigin(CGRect rect, CGFloat originX, CGFloat originY)
{
    rect.origin.x = originX;
    rect.origin.y = originY;
    return rect;
}

// Назначить rect новый size
CG_INLINE CGRect CGRectSize(CGRect rect, CGFloat width, CGFloat height);

CG_INLINE CGRect
CGRectSize(CGRect rect, CGFloat width, CGFloat height)
{
    rect.size.width = width;
    rect.size.height = height;
    return rect;
}

// Назначить rect новый size.width
CG_INLINE CGRect CGRectSizeWidth(CGRect rect, CGFloat width);

CG_INLINE CGRect
CGRectSizeWidth(CGRect rect, CGFloat width)
{
    rect.size.width = width;
    return rect;
}

// Назначить rect новый size.height
CG_INLINE CGRect CGRectSizeHeight(CGRect rect, CGFloat height);

CG_INLINE CGRect
CGRectSizeHeight(CGRect rect, CGFloat height)
{
    rect.size.height = height;
    return rect;
}

// Увеличить size на dw по ширине и на dh по высоте
CG_INLINE CGSize CGSizeIncrease(CGSize size, CGFloat dw, CGFloat dh);

CG_INLINE CGSize
CGSizeIncrease(CGSize size, CGFloat dw, CGFloat dh)
{
    size.width += dw;
    size.height += dh;
    return size;
}


#endif /* UIHelper_h */
