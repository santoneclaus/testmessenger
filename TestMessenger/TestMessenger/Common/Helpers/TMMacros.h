//
//  TMMacros.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#ifndef TMMacros_h
#define TMMacros_h

#define TMAssert(CONDITION)                                \
    if (!(CONDITION)) {                                       \
        NSAssert(false, @"Activation assert: " @ #CONDITION);   \
    }

#define TMAssertRet(CONDITION, RET_VALUE)                  \
    if (!(CONDITION)) {                                       \
        NSAssert(false, @"Activation assert: " @ #CONDITION);   \
        return RET_VALUE;                                       \
    }
#define TMAssertRetVoid(CONDITION) TMAssertRet(CONDITION, )

#define TMRequires(CONDITION, RET_VALUE)                       \
    if (!(CONDITION)) {                                           \
        return RET_VALUE;                                           \
    }
#define TMRequiresType(VALUE, TYPE, RET_VALUE) TMRequires(nil != VALUE && [VALUE isKindOfClass:[TYPE class]], RET_VALUE)

#define TMSafeInit(INIT_CONSTRUCTOR, RET_VALUE) \
    TMAssertRet(nil != (INIT_CONSTRUCTOR), RET_VALUE)

#define TMSafeInitSelf(INIT_CONSTRUCTOR) TMSafeInit(INIT_CONSTRUCTOR, self)

#define TMSafeInitWithNibSelf TMSafeInitSelf(self = [super initWithNibName:NSStringFromClass([self class]) bundle:[NSBundle myLibraryResourcesBundle]])

#define TMWeakify(var) __weak typeof(var) TMWeak_##var = var;

#define TMStrongify(var)\
    _Pragma("clang diagnostic push") _Pragma("clang diagnostic ignored \"-Wshadow\"") __strong typeof(var) var = TMWeak_##var; \
    _Pragma("clang diagnostic pop")

#define TMCheckNull(VALUE) ((VALUE) == nil || (VALUE) == [NSNull null])
#define TMUnEscapeNull(VALUE) ((VALUE) == [NSNull null] ? nil : (VALUE))

#define TMEscapeNilDefault(VALUE, DEFAULT) ((VALUE) == nil ? DEFAULT : (VALUE))
#define TMEscapeNil(VALUE) TMEscapeNilDefault(VALUE, [NSNull null])

#define TMCheckEmpty(VALUESTR) ((VALUESTR) == nil || (VALUESTR).length == 0)

#define TMCheckRet(CONDITION, RET_VALUE)            \
    if (CONDITION)                                                   \
        return RET_VALUE;

#define TMCheckRetVoid(CONDITION) TMCheckRet(CONDITION, )

#define TMEscapeNilDefault(VALUE, DEFAULT) ((VALUE) == nil ? DEFAULT : (VALUE))

#define TMCheckContinue(CONDITION) \
    if (CONDITION)                  \
        continue;

#endif /* TMMacros_h */
