//
//  GDCHelper.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "GDCHelper.h"

@implementation GDCHelper

void runOnMainQueueWithoutDeadlocking(void (^block)(void)) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

void runOnMainQueue(void (^block)(void)) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

@end
