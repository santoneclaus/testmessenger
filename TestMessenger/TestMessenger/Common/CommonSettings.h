//
//  CommonSettings.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SendMessageEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommonSettings : NSObject

+ (instancetype)sharedInstance;
+ (NSString *)baseURL;
+ (SendMessageEntity *)sendMessageEntityWithText:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
