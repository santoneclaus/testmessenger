//
//  TMChatRouter.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChatProtocols.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMChatRouter : NSObject <TMChatRouterProtocol>

@end

NS_ASSUME_NONNULL_END
