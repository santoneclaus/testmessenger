//
//  TMChatViewController.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMChatProtocols.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMChatViewController : UIViewController <TMChatViewProtocol>
@property (nonatomic, strong) id <TMChatPresenterProtocol>presenter;

@end

NS_ASSUME_NONNULL_END
