//
//  TMChatTableConfigurator.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMChatTableConfigurator.h"
#import "TMDialogTableViewCell.h"
#import "MessageEntity.h"
#import "TMUpdateTimeService.h"

static TMDialogTableViewCell *staticCell;

@interface TMChatTableConfigurator () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <MessageEntity*>* messages;
@property (nonatomic, strong) TMUpdateTimeService *updateTimeService;
@property (nonatomic, strong) NSString *staticCellId;

@property (nonatomic, strong) NSObject *monitor;
@property (nonatomic, assign) NSUInteger elementsCount;

@end

@implementation TMChatTableConfigurator

- (instancetype)initWithTableView:(UITableView *)tableView {
    TMSafeInitSelf(self = [super init]);

    self.monitor = [[NSObject alloc] init];
    self.staticCellId = @"staticCellId";
    self.elementsCount = 0;
    
    self.tableView = tableView;
     [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TMDialogTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([TMDialogTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TMDialogTableViewCell class]) bundle:nil] forCellReuseIdentifier:self.staticCellId];
    self.messages = [NSMutableArray array];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    TMWeakify(self);
    self.updateTimeService = [[TMUpdateTimeService alloc] initWithCallBack:^BOOL{
        TMStrongify(self);
        [self updateTime];
        return NO;
    }];
    
    return self;
}

- (void)addMessage:(MessageEntity *)message {
    @synchronized (self.monitor) {
        [self.messages insertObject:message
                            atIndex:0];
        self.elementsCount = self.messages.count - 1;
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//        [self.tableView setContentOffset:CGPointZero animated:YES];
        [self.updateTimeService reupdateTable:self.tableView];
    }
}

- (void)dealloc {
    [self.updateTimeService stoopUpdateTable];
}

- (void)updateTime {
    @synchronized (self.monitor) {
        NSArray *cells = [self.tableView visibleCells];
        NSMutableArray *identifiers = [NSMutableArray array];
        for (TMDialogTableViewCell *cell in cells) {
            NSUInteger row = self.elementsCount - cell.row;
            TMCheckContinue(self.messages.count <= row);
            MessageEntity *message = self.messages[row];
            cell.timeLabel.text = [message getTimeString];
            [cell updateDateLabelWidth];
            if ([cell needUpdateHeight]) {
                [identifiers addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
        }
        if (0 < identifiers.count) {
            [self.tableView reloadRowsAtIndexPaths:identifiers withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (nil == staticCell) {
        staticCell = [self.tableView dequeueReusableCellWithIdentifier:self.staticCellId];
        staticCell.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 700);
        staticCell.contentView.frame = staticCell.bounds;
    }
//#warning dictionary
    return [self configureCell:staticCell indexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

- (MessageEntity *)messageWithRow:(NSInteger)row {
    @synchronized (self.monitor) {
        MessageEntity *message = nil;
        if (row < self.messages.count) {
            message = self.messages[row];
        }
        return message;
    }
}

- (CGFloat)configureCell:(TMDialogTableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    cell.row = self.elementsCount - indexPath.row;
    return [cell setContentWithMessageEntity:[self messageWithRow:indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMDialogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass( [TMDialogTableViewCell class])];
    
    [self configureCell:cell indexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
