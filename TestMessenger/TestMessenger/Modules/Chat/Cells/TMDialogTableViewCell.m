//
//  TMDialogTableViewCell.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMDialogTableViewCell.h"
static CGFloat dialogCellBubleSpace = 40;

@interface TMDialogTableViewCell()

@property (nonatomic, weak) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textRightSpaceConstraint;
@property (nonatomic, weak) IBOutlet UIView *arrowView;

@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) CGFloat timeLabelHeight;
@property (nonatomic, assign) BOOL needUpdateHeight;

@end

@implementation TMDialogTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.timeLabel.minimumScaleFactor = 0.9;
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    self.timeLabelHeight = self.timeLabel.frame.size.height;
    self.needUpdateHeight = NO;
    TMCheckRetVoid(nil == self.arrowView);
    self.messageLabel.adjustsFontSizeToFitWidth = YES;
    self.messageLabel.minimumScaleFactor = 0.5;
    self.arrowView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_4);
    
    self.transform = CGAffineTransformRotate(self.transform, M_PI);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *selectedBackgroundView = [[UIView alloc] init];
    selectedBackgroundView.backgroundColor = [UIColor clearColor];
    self.selectedBackgroundView = selectedBackgroundView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - TMUpdateTimeDelegate
- (void)updateDate {
    
}

#pragma mark - content
- (CGFloat)setContentWithMessageEntity:(MessageEntity *)messageEntity {
    CGFloat height = 40;
    TMCheckRet(nil == messageEntity, height);
    NSString *text = messageEntity.message;
    NSString *date = [messageEntity getTimeString];
    height = [self setText:text date:date];

    return height;
}

- (CGFloat)setText:(NSString *)text date:(NSString *)date {
    if (10 > self.timeLabelHeight) {
        self.timeLabelHeight = self.timeLabel.frame.size.height;
    }
    
    CGFloat space = self.subContentView.frame.origin.y;
    CGFloat height = space*4;
    self.messageLabel.text = text ? : @"";
    self.timeLabel.text = date ? : @"";
    CGRect labelFrame = self.timeLabel.frame;
    [self.timeLabel sizeToFit];
    labelFrame.origin.x = labelFrame.origin.x - (labelFrame.size.width - self.timeLabel.frame.size.width);
    self.timeLabel.frame = CGRectSizeWidth(labelFrame, self.timeLabel.frame.size.width);
    self.timeLabelWidthConstraint.constant = self.timeLabel.frame.size.width;
    

    CGFloat width = MAIN_WIDTH - 4*self.messageLabel.frame.origin.x - dialogCellBubleSpace;
    self.messageLabel.frame = CGRectSize(self.messageLabel.frame, 1000, self.timeLabel.frame.size.height);
    [self.messageLabel sizeToFit];
    height += ceil(self.timeLabel.frame.size.height);
    CGFloat maxLabelWidth = (width - self.timeLabelWidthConstraint.constant - 2*space);
    if ((self.messageLabel.frame.size.width <= maxLabelWidth) && (self.messageLabel.frame.size.height < ceil(self.timeLabel.frame.size.height) + 5)) {
        self.textRightSpaceConstraint.constant = self.timeLabelWidthConstraint.constant + 2*space;

        self.contentSpaceConstraint.constant = self.messageLabel.frame.origin.x + dialogCellBubleSpace + width - self.messageLabel.frame.size.width - self.textRightSpaceConstraint.constant - space;
    } else {
        self.textRightSpaceConstraint.constant = space;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.frame = CGRectSize(self.messageLabel.frame, width, 1000.0);
        [self.messageLabel sizeToFit];
        self.contentSpaceConstraint.constant = dialogCellBubleSpace + MAX(0, width - MAX(self.messageLabel.frame.size.width, self.timeLabel.frame.size.width) - 5);
        height += space + self.messageLabel.frame.size.height;
    }
    self.cellHeight = height;
    [self layoutIfNeeded];
    return height;
}

- (void)updateDateLabelWidth {
    CGFloat cellHeight = self.cellHeight;
    [self setText:self.messageLabel.text date:self.timeLabel.text];
    self.needUpdateHeight = (cellHeight != self.cellHeight);
}

@end
