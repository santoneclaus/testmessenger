//
//  TMDialogTableViewCell.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMUpdateTimeService.h"
#import "MessageEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDialogTableViewCell : UITableViewCell <TMUpdateTimeDelegate>
@property (assign, nonatomic) NSInteger row;
@property (weak, nonatomic, readonly) UILabel *timeLabel;

- (CGFloat)setContentWithMessageEntity:(MessageEntity *)messageEntity;

- (CGFloat)setText:(NSString *)text date:(NSString *)date;

- (void)updateDateLabelWidth;

- (BOOL)needUpdateHeight;



@end

NS_ASSUME_NONNULL_END
