//
//  TMChatProtocols.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#ifndef TMChatProtocols_h
#define TMChatProtocols_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MessageEntity.h"

@protocol TMChatPresenterProtocol;
@protocol TMChatViewProtocol//PresenterToViewChatProtocol
@property (nonatomic, strong) id <TMChatPresenterProtocol>presenter;

- (void)showHUD;
- (void)hideHUD;
- (void)addMessage:(MessageEntity *)message;
- (void)showError:(NSString *)errorText;
@end

@protocol TMChatPresenterProtocol;
@protocol TMChatInteractorProtocol//PresenterToInteractorChatProtocol
@property (nonatomic, weak) id <TMChatPresenterProtocol>presenter;

- (void)sendMessage:(NSString *)message;
@end

@protocol TMChatRouterProtocol;
@protocol TMChatPresenterProtocol//ViewToPresenterChatProtocol + InteractorToPresenterChatProtocol
@property (nonatomic, strong) id <TMChatRouterProtocol>router;
@property (nonatomic, strong) id <TMChatInteractorProtocol>interactor;
@property (nonatomic, assign) id <TMChatViewProtocol>view;

//ViewToPresenterChatProtocol
- (void)sendMessage:(NSString *)message;
//InteractorToPresenterChatProtocols
- (void)addMessage:(MessageEntity *)message;
@end

@protocol TMChatRouterProtocol//PresenterToRouterChatProtocol
// нет других модулей
@end

#endif /* TMChatProtocols_h */
