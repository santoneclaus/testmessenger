//
//  TMChatViewController.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMChatViewController.h"
#import "CommentView.h"
#import "TMKeyboardAppearanceTracker.h"
#import "TMChatTableConfigurator.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "TMChatConfigurator.h"

@interface TMChatViewController ()

@property (weak, nonatomic) IBOutlet CommentView *commentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottonConstraint;

@property (nonatomic, strong) TMKeyboardAppearanceTracker *keyboardAppearanceTracker;
@property (nonatomic, strong) TMChatTableConfigurator *tableConfigurator;

@end

@implementation TMChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableConfigurator = [[TMChatTableConfigurator alloc] initWithTableView:self.tableView];
    
    [self configureUI];
    
    if (nil == self.presenter) {
        [TMChatConfigurator configureModuleWithView:self];
    }
}
#pragma mark - configure
- (void)configureTableView {
    [self.tableView setTableHeaderView:[[UIView alloc] initWithFrame:CGRectZero]];
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = RGBA(237, 237, 237, 1);
    self.tableView.backgroundView = backgroundView;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.rowHeight = 80;
    
    //set transform
    self.tableView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0 - 50, 0, MAIN_WIDTH - 10);
    
    UIEdgeInsets contentInset = UIEdgeInsetsMake(self.tableView.contentInset.bottom, self.tableView.contentInset.right, self.tableView.contentInset.top, self.tableView.contentInset.left);
    self.tableView.contentInset = contentInset;
}

- (void)configureCommentView {
//    self.commentView.disableAnimation = YES;
    //add bottom space
    if (2436 <= [[UIScreen mainScreen] nativeBounds].size.height) {
        self.commentView.bottomSpaceConstraint.constant += 30;
        self.keyboardAppearanceTracker.bottomSpace = 30;
        [self.commentView updateHeight];
    }
    TMWeakify(self);
    [self.commentView setSendCommentCallBack:^(NSString *comment){
        TMStrongify(self);
        [self sendComment:comment];
    }];
}

- (void)configureUI {
    [self configureTableView];
    
    self.keyboardAppearanceTracker = [[TMKeyboardAppearanceTracker alloc] initWithHeightConstraint:self.bottonConstraint parent:self.tableView];
    self.keyboardAppearanceTracker.bottomSpace = 0;

    [self configureCommentView];
}

#pragma mark - actions
- (void)sendComment:(NSString *)comment {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    TMCheckRetVoid(nil == self.presenter);
    
    [self.presenter sendMessage:comment];
}

- (IBAction)tapOnView:(UITapGestureRecognizer *)gesture {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

#pragma mark - TMChatViewProtocol
- (void)showHUD {
    //TODO: добавлять временные сообщения в список, не блокировать
    [self.commentView setDisable:YES];
    [SVProgressHUD showWithStatus:@"Отправка сообщения"];
}

- (void)hideHUD {
    //TODO: добавлять временные сообщения в список, не блокировать
    [self.commentView setDisable:NO];
    [SVProgressHUD dismiss];
}

- (void)addMessage:(MessageEntity *)message {
    TMCheckRetVoid(nil == message);
    //TODO: check value
    [self.commentView clearComment];
    [self.tableConfigurator addMessage:message];
}

- (void)showError:(NSString *)errorText {
    TMCheckRetVoid(nil == errorText);
    [SVProgressHUD showErrorWithStatus:errorText];
}

@end
