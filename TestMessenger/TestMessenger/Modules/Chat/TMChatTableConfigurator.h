//
//  TMChatTableConfigurator.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 22.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMChatTableConfigurator : NSObject

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithTableView:(nonnull UITableView *)tableView;
- (void)addMessage:(nonnull MessageEntity *)message;

@end

NS_ASSUME_NONNULL_END
