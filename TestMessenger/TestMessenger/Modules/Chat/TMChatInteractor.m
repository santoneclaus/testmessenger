//
//  TMChartInteractor.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMChatInteractor.h"
#import "TMSendMessageService.h"
#import "CommonSettings.h"

@interface TMChatInteractor ()

@property (nonatomic, strong) TMSendMessageService* service;

@end

@implementation TMChatInteractor

- (instancetype)init {
    TMSafeInitSelf(self = [super init]);
    
    self.service = [[TMSendMessageService alloc] initWithUrlString:[CommonSettings baseURL]];
    
    return self;
}

- (void)sendMessage:(NSString *)message {
    TMWeakify(self);
    SendMessageEntity *sendMessageEntity = [CommonSettings sendMessageEntityWithText:message];
    [self.service sendMessageWithEntity:sendMessageEntity
                               callBack:^(MessageEntity * _Nonnull messageEntity) {
        TMStrongify(self);
        if (nil != self.presenter) {
            [self.presenter addMessage:messageEntity];
        }
    }];
}

@end
