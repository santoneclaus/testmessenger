//
//  TMChatPresenter.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMChatPresenter.h"

@implementation TMChatPresenter

- (void)sendMessage:(NSString *)message {
    TMCheckRetVoid(nil == self.interactor || nil == self.view);
    [self.view showHUD];
    [self.interactor sendMessage:message];
}

- (void)addMessage:(MessageEntity *)message {
    TMCheckRetVoid(nil == self.view);
    [self.view hideHUD];
    TMCheckRetVoid(nil == message);
    if (0 == message.status) {
        [self.view addMessage:message];
    } else {
        [self.view showError:message.error];
    }
}

@end
