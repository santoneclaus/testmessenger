//
//  TMChatPresenter.h
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChatProtocols.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMChatPresenter : NSObject <TMChatPresenterProtocol>

@property (nonatomic, strong) id <TMChatRouterProtocol>router;
@property (nonatomic, strong) id <TMChatInteractorProtocol>interactor;
@property (nonatomic, assign) id <TMChatViewProtocol>view;

@end

NS_ASSUME_NONNULL_END
