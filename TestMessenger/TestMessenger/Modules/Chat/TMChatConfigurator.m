//
//  TMChatConfigurator.m
//  TestMessenger
//
//  Created by Alexandr Dubachev on 23.03.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "TMChatConfigurator.h"
#import "TMChatInteractor.h"
#import "TMChatPresenter.h"
#import "TMChatRouter.h"

@implementation TMChatConfigurator

+ (void)configureModuleWithView:(id<TMChatViewProtocol>)view {
    
    TMChatInteractor *interactor = [[TMChatInteractor alloc] init];
    TMChatPresenter *presenter = [[TMChatPresenter alloc] init];
    TMChatRouter *router = [[TMChatRouter alloc] init];

    view.presenter = presenter;
    presenter.view = view;
    
    interactor.presenter = presenter;
    presenter.interactor = interactor;
    
    presenter.router = router;
}

@end
